# PF ansible role
Configure [PF](https://docs.freebsd.org/en/books/handbook/firewalls/#firewalls-pf) on FreeBSD. The [pf syntax](https://www.openbsd.org/faq/pf/filter.html) is already quite elegant and succinct so this ansible role does little more than take   lists and dictionaries of macros, tables and rules and copy them into place.

## Usage
- `pf_logging`: Whether or not logging is enabled. Defaults to `false`.
- `pf_tables`: Dictionary containing tables to be created on the host. The key of each list in `pf_tables` is the name of a table that is able to be reference in `pf_rules`.
- `pf_macros`: Dictionary containing macros to be created on the host. The key of each list and/or field in `pf_macros` is the name of a macro that is able to be reference in `pf_rules`.
- `pf_rules`: A list of rules to apply to pf

## Examples
```yaml
---
pf_logging: true

pf_macros:
  # External interface
  wanint: igb0
  # Internal interfaces
  lanints:
    - igb1.2
    - igb1.2
    - igb1.3
    - igb1.4
    - igb1.5
    - igb1.99
  # Wireguard interfaces
  wgints:
    - wg0
    - wg1
  # Wireguard listen ports
  wgports:
    - 5764
    - 5765
  routetointernet:
    - <routetointernet>

pf_tables:
  adminnetworks:
    - 10.50.50.0/24
    - 172.45.41.0/24
  routetointernet:
    - 172.45.3.0/24
    - 172.45.40.0/24
    - 10.50.8.0/24
    - 10.50.9.0/24
  routetoservices:
    - 172.45.3.0/24
    - 172.45.40.0/24
    - 10.50.5.0/24


pf_rules:
  # NAT match rule
  - "nat on $wanint from { <routetointernet> <adminnetworks> } -> ($wanint)"
  # Default deny
  - "block log all"
  # Allow through wireguard and loopback interfaces
  - "pass quick on lo0 no state"
  - "pass quick on $wgints"
  # Allow DHCP traffic from internal networks
  - "pass in quick on $lanints proto udp from port 68 to port 67"
  - "pass out quick on $lanints proto udp from port 67 to port 68"
  # Allow DNS traffic from internal networks
  - "pass in quick on $lanints proto udp to port 53 keep state"
  # Allow through the wireguard ports
  - "pass in quick on $wanint proto udp from any to $wanint port $wgports"
  # Allow admin networks to route to everything
  - "pass from <adminnetworks>"
  # Allow hosts with internet access out of the external interfaces
  - "pass out on $wanint from { <routetointernet> <adminnetworks> }"
  - "pass on $wanint from 10.0.0.0/8"
```
